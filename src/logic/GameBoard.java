package logic;

import java.awt.event.KeyListener;

public class GameBoard {

	private int[][] grid;
	private int index;
	private int columns, rows;
	private static final int PLAYER_1 = 1;
	private int lastY, openSlots;
	private int counter;

	public GameBoard(int columns, int rows) {
		this.columns = columns;
		this.rows = rows;
		grid = new int[columns][rows];
		index = 0;
		openSlots = columns * rows;
		counter = 0;
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				grid[i][j] = 0;
			}
		}
	}

	/**
	 * Resets the grid for when a new game is started
	 */
	public void clear() {
		openSlots = columns * rows;
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				grid[i][j] = 0;
			}
		}
	}

	/**
	 * Moving one step to the left on the game board.
	 */
	public void moveLeft() {
		index -= 1;
		if (index < 0) {
			index = columns - 1;
		}
	}

	/**
	 * Moving one step to the right on the game board.
	 */
	public void moveRight() {
		index += 1;
		if (index >= columns) {
			index = 0;
		}
	}

	/**
	 * 
	 * @param Player
	 *            decides if a red or yellow disk should be dropped
	 * @return true if a disc was successfully placed
	 */
	public boolean dropDisc(int Player) {
		if (Player == PLAYER_1) {
			return dropRedDisc(index);
		} else {
			return dropYellowDisc(index);
		}

	}

	private boolean dropYellowDisc(int column) {
		for (int j = rows - 1; j >= 0; j--) {
			if (grid[column][j] == 0) {
				grid[column][j] = -1;
				lastY = j;
				openSlots--;
				return true;
			}
		}
		return false;
	}

	private boolean dropRedDisc(int column) {
		for (int j = rows - 1; j >= 0; j--) {
			if (grid[column][j] == 0) {
				grid[column][j] = 1;
				lastY = j;
				openSlots--;
				return true;
			}
		}
		return false;
	}

	public int getIndex() {
		return index;
	}

	public int getColumns() {
		return columns;
	}

	public int getRows() {
		return rows;
	}

	public int[][] getGrid() {
		return grid;
	}

	/**
	 * Checks the board to see if the player who last placed a disc has gotten
	 * four in a row
	 * 
	 * @param player
	 *            who placed the disc, currently -1 or 1
	 * @return the outcome. 0 if no winner found, 5 if there is a draw and 1 &
	 *         -1 if either player won.
	 */
	public int checkBoard(int player) {
		if (openSlots <= 0) {
			System.out.println("I's a draw");
			return 5;
		}
		if (checkHorizontal(player) || checkVertical(player)
				|| checkDiagonal(player)) {
			return player;
		}

		return 0;
	}

	/**
	 * Checking the entire grid both diagonal ways. Could be improved by using
	 * the slot the latest disked dropped in to as a point of reference.
	 */
	private boolean checkDiagonal(int player) {

		// top-left to bottom-right
		for (int rowStart = 0; rowStart < rows - 4; rowStart++) {
			counter = 0;
			int row, col;
			for (row = rowStart, col = 0; row < rows && col < columns; row++, col++) {
				if (grid[col][row] == player) {
					counter++;
					if (counter >= 4) {
						System.out.println("Diagonal win for player" + player);
						return true;
					}
				} else {
					counter = 0;
				}
			}
		}

		// top-left to bottom-right
		for (int colStart = 1; colStart < columns - 4; colStart++) {
			counter = 0;
			int row, col;
			for (row = 0, col = colStart; row < rows && col < columns; row++, col++) {
				if (grid[col][row] == player) {
					counter++;
					if (counter >= 4) {
						System.out.println("Diagonal win for player" + player);
						return true;
					}
				} else {
					counter = 0;
				}

			}

		}
		// Bottom left to top-right
		for (int rowStart = rows - 1; rowStart > 2; rowStart--) {
			counter = 0;
			int row, col;
			for (row = rowStart, col = 0; row >= 0 && col < columns; row--, col++) {
				if (grid[col][row] == player) {
					counter++;
					if (counter >= 4) {
						System.out.println("Diagonal win for player" + player);
						return true;
					}
				} else {
					counter = 0;
				}
			}
		}

		// Bottom-left to top-right
		for (int colStart = 1; colStart < columns - 3; colStart++) {
			counter = 0;
			int row, col;
			for (row = rows - 1, col = colStart; row >= 0 && col < columns; row--, col++) {
				if (grid[col][row] == player) {
					counter++;
					if (counter >= 4) {
						System.out.println("Diagonal win for player" + player);
						return true;
					}
				} else {
					counter = 0;
				}

			}

		}
		return false;
	}

	/**
	 * Uses the position of the last disc placed and only checks in a vertical
	 * line from where it dropped
	 * 
	 * @param player
	 * @return
	 */
	private boolean checkVertical(int player) {
		counter = 0;
		int up = lastY - 3;
		int down = lastY + 3;
		if (up < 0)
			up = 0;
		if (down >= rows)
			down = rows - 1;
		for (int i = up; i <= down; i++) {
			if (grid[index][i] == player) {
				counter++;
				if (counter == 4) {
					System.out.println("Vertical win for Player " + player);
					return true;

				}
			} else {
				counter = 0;
			}
		}
		return false;
	}

	/**
	 * Uses the position of the last disc placed and only checks in a horizontal
	 * line from where it dropped
	 * 
	 * @param player
	 * @return
	 */
	private boolean checkHorizontal(int player) {
		counter = 0;
		int left = index - 3;
		int right = index + 3;
		if (left < 0)
			left = 0;
		if (right >= columns)
			right = columns - 1;
		for (int i = left; i <= right; i++) {
			if (grid[i][lastY] == player) {
				counter++;
				if (counter == 4) {
					System.out.println("Horizontal win for Player " + player);
					return true;
				}
			} else {
				counter = 0;
			}
		}
		return false;
	}

	/**
	 * Only used for debugging the program for further development
	 */
	public void printBoard() {
		System.out.println(" 1  2  3  4  5  6  7");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print("[" + grid[j][i] + "]");
			}
			System.out.println();
		}
		System.out.println("\n");
	}
}
