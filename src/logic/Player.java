package logic;


/**
 * Player class created in a way that suits the SQL database well. 
 * @author Simon
 *
 */

public class Player {
	private String name;
	private int games, wins, losses, draws;

	public Player(String name, int games, int wins, int losses, int draws) {
		this.name = name;
		this.games = games;
		this.wins = wins;
		this.losses = losses;
		this.draws = draws;
	}

	public String getName() {
		return name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public int getGames() {
		return games;
	}

	public void playedGame() {
		games++;
	}

	public int getWins() {
		return wins;
	}

	public void won() {
		wins++;
	}

	public int getLosses() {
		return losses;
	}

	public void lost() {
		losses++;
	}

	public int getDraws() {
		return draws;
	}

	public void draw() {
		draws++;
	}

	public double getRatio() {
		if (losses != 0) {
			return (double) wins / (double) losses;
		} else {
			return wins;
		}
	}

}