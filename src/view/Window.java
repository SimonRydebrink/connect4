package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import logic.GameBoard;
import logic.Player;
import main.Game;

public class Window {
	public static final int PLAY_STATE = 0, P1_WON = 1, P2_WON = -1,
			LOGIN_STATE = 2, HIGHSCORE_STATE = 3, NEW_GAME = 4, DRAW_GAME = 5;
	public static int STATE;
	private JLabel[][] slots;
	private JLabel p1Stats, p1Wins, p1Losses, p1Draws, p1Ratio, p2Stats,
			p2Wins, p2Losses, p2Draws, p2Ratio;
	private GameBoard gb;
	private Font statsFont = new Font("Verdana", 1, 14);
	private int player = 1;
	public static Player p1, p2;

	public Window(int width, int height, int columns, int rows, String title) {
		STATE = PLAY_STATE;
		JFrame frame = new JFrame(title);
		JFrame scoreFrame = new JFrame("Highscore");

		JTextArea nameField = new JTextArea();
		JTextArea winsField = new JTextArea();
		JTextArea lossesField = new JTextArea();
		nameField.setEditable(false);
		winsField.setEditable(false);
		lossesField.setEditable(false);
		scoreFrame.add(nameField, BorderLayout.WEST);
		scoreFrame.add(winsField, BorderLayout.CENTER);
		scoreFrame.add(lossesField, BorderLayout.EAST);
		scoreFrame.setFocusable(false);
		JMenuBar menu = new JMenuBar();
		JPanel gameBoard = new JPanel();
		JPanel buttons = new JPanel();
		JPanel playerOneInfo = new JPanel();
		playerOneInfo.setLayout(new GridLayout(10, 1));
		JPanel playerTwoInfo = new JPanel();
		playerTwoInfo.setLayout(new GridLayout(10, 1));
		playerOneInfo.add(new LoginP1Button());
		playerTwoInfo.add(new LoginP2Button());
		p1 = new Player("Player 1", 0, 0, 0, 0);
		p2 = new Player("Player 2", 0, 0, 0, 0);

		p1Stats = new JLabel();
		p1Wins = new JLabel();
		p1Losses = new JLabel();
		p1Draws = new JLabel();
		p1Ratio = new JLabel();

		p2Stats = new JLabel();
		p2Wins = new JLabel();
		p2Losses = new JLabel();
		p2Draws = new JLabel();
		p2Ratio = new JLabel();

		playerOneInfo.add(p1Stats);
		playerOneInfo.add(p1Wins);
		playerOneInfo.add(p1Losses);
		playerOneInfo.add(p1Draws);
		playerOneInfo.add(p1Ratio);
		playerTwoInfo.add(p2Stats);

		playerTwoInfo.add(p2Wins);
		playerTwoInfo.add(p2Losses);
		playerTwoInfo.add(p2Draws);
		playerTwoInfo.add(p2Ratio);

		gb = new GameBoard(columns, rows);

		JMenu options = new JMenu("Options");
		menu.add(options);
		frame.add(buttons, BorderLayout.SOUTH);
		gameBoard.setLayout(new GridLayout(rows + 1, columns));
		slots = new JLabel[rows + 1][columns];

		frame.add(menu, BorderLayout.NORTH);
		frame.add(playerOneInfo, BorderLayout.WEST);
		frame.add(playerTwoInfo, BorderLayout.EAST);
		frame.add(gameBoard, BorderLayout.CENTER);
		gameBoard.setBackground(new Color(240, 240, 220));
		for (int i = 0; i < columns; i++) {
			slots[0][i] = new JLabel();
			slots[0][i].setHorizontalAlignment(SwingConstants.CENTER);
			slots[0][i].setBorder(new LineBorder(new Color(240, 240, 220)));
			slots[0][i].setOpaque(true);
			slots[0][i].setBackground(Color.WHITE);
			gameBoard.add(slots[0][i]);
		}
		for (int i = 1; i <= rows; i++) {
			for (int j = 0; j < columns; j++) {
				slots[i][j] = new JLabel();
				slots[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				slots[i][j].setBorder(new LineBorder(Color.black));
				gameBoard.add(slots[i][j]);
			}
		}
		frame.setPreferredSize(new Dimension(width, height));
		frame.setMinimumSize(new Dimension(width, height));

		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		scoreFrame.setPreferredSize(new Dimension(width / 2, height));
		scoreFrame.setMinimumSize(new Dimension(width / 2, height));

		scoreFrame.setResizable(false);
		scoreFrame.setLocationRelativeTo(null);
		scoreFrame.setVisible(false);
		scoreFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		/**
		 * Just a way of saving the progress when closing the application. Still
		 * need to find a solution to when you don't want to close the program
		 */
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(frame,
						"Are you sure to close this window?",
						"Really Closing?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					saveAndQuit();
					System.exit(0);
				} else {
					// NEED TO KEEP THE WINDOW OPEN
				}
			}
		});

		gameBoard.setFocusable(true);
		gameBoard.addKeyListener(new KeyListener() {
			/**
			 * Game controlled with the keyboard
			 */
			@Override
			public void keyPressed(KeyEvent arg0) {
				int KEY = arg0.getKeyCode();
				if (KEY == KeyEvent.VK_LEFT) {
					gb.moveLeft();
				} else if (KEY == KeyEvent.VK_RIGHT) {
					gb.moveRight();
				} else if (KEY == KeyEvent.VK_ENTER) {
					if (gb.dropDisc(player)) {
						STATE = gb.checkBoard(player);
						player = player * -1;
					} else {
						System.out.println("Column " + (gb.getIndex() + 1)
								+ " is full");
					}
				} else if (KEY == KeyEvent.VK_ESCAPE) {
					// Change Game state to start menu.
					toggleScore();
					System.out.println("Start Menu");
				}
				update(gb);

				switch (STATE) {
				case PLAY_STATE:
					break;
				case HIGHSCORE_STATE:
					break;
				case NEW_GAME:
					gb.clear();
					update(gb);
					break;
				case P1_WON:
					p1.won();
					p2.lost();
					displayWinScreen(p1);
					break;
				case P2_WON:
					p2.won();
					p1.lost();
					displayWinScreen(p2);
					break;
				case DRAW_GAME:
					p1.draw();
					p2.draw();
					displayDrawScreen();
				}
				updateStats();
			}

			/**
			 * Pop-up to let users know that the game came to a draw Option to
			 * start new game offered
			 */
			private void displayDrawScreen() {
				int answer = JOptionPane.showConfirmDialog(null,
						"It's a draw! \nNew game?", "DRAW!",
						JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.YES_OPTION) {
					JOptionPane.showMessageDialog(null, "NEW GAME STARTED");
					STATE = PLAY_STATE;
					gb.clear();
					update(gb);

				} else {
					JOptionPane.showMessageDialog(null, "Fair enough");
					saveAndQuit();
					System.exit(0);
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}

			/**
			 * Pop-up to let users know that the is over and that someone won
			 * Option to start new game offered
			 */
			private void displayWinScreen(Player p) {
				int answer = JOptionPane.showConfirmDialog(null, p.getName()
						+ " is the Winner! \nNew game?", "WINNER!",
						JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.YES_OPTION) {
					JOptionPane.showMessageDialog(null, "NEW GAME STARTED");
					STATE = PLAY_STATE;
					gb.clear();
					update(gb);

				} else {
					JOptionPane.showMessageDialog(null, "Fair enough");
					saveAndQuit();
					System.exit(0);
				}
			}

			/**
			 * Updating and accessing the database in order to print get the
			 * highscore
			 */
			private void toggleScore() {
				scoreFrame.setVisible(true);
				try {
					int place = 1;
					StringBuilder names = new StringBuilder();
					StringBuilder win = new StringBuilder();
					StringBuilder loss = new StringBuilder();

					Connection myCon = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/Connect4", "admin",
							"admin");

					Statement myStmt = myCon.createStatement();

					for (int i = 0; i < Game.NEWPLAYERS.size(); i++) {
						Player p = Game.NEWPLAYERS.get(i);
						String insert = "insert into players values ('"
								+ p.getName() + "', " + p.getGames() + ", "
								+ p.getWins() + ", " + p.getLosses() + ", "
								+ p.getDraws() + ")";
						myStmt.executeUpdate(insert);
					}
					for (int i = 0; i < Game.PLAYERS.size(); i++) {
						Player p = Game.PLAYERS.get(i);
						String update = "update players set games = "
								+ p.getGames() + ", wins = " + p.getWins()
								+ ", losses = " + p.getLosses() + ", draws = "
								+ p.getDraws() + " where username ='"
								+ p.getName() + "'";
						myStmt.executeUpdate(update);
					}
					names.append("PLAYER \n");
					win.append("      " + "WINS \n");
					loss.append("LOSSES \n");
					ResultSet myRs = myStmt
							.executeQuery("select * from players order by wins desc");
					while (myRs.next() && place < 15) {
						String name = myRs.getString("username");
						int wins = myRs.getInt("wins");
						int losses = myRs.getInt("losses");
						int draws = myRs.getInt("draws");
						names.append(place + ". " + name + "\n");
						win.append("      " + wins + "\n");
						loss.append(losses + "\n");
						place++;
					}
					nameField.setText(names.toString());
					winsField.setText(win.toString());
					lossesField.setText(loss.toString());

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public void keyTyped(KeyEvent arg0) {

			}
		});

		update(gb);
		updateStats();

	}

	/**
	 * Saves new created users and updates old ones
	 * 
	 * @return
	 */
	private int saveAndQuit() {
		try {
			Connection myCon = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/Connect4", "admin", "admin");

			Statement myStmt = myCon.createStatement();

			for (int i = 0; i < Game.NEWPLAYERS.size(); i++) {
				Player p = Game.NEWPLAYERS.get(i);
				String insert = "insert into players values ('" + p.getName()
						+ "', " + p.getGames() + ", " + p.getWins() + ", "
						+ p.getLosses() + ", " + p.getDraws() + ")";
				myStmt.executeUpdate(insert);
			}
			for (int i = 0; i < Game.PLAYERS.size(); i++) {
				Player p = Game.PLAYERS.get(i);
				String update = "update players set games = " + p.getGames()
						+ ", wins = " + p.getWins() + ", losses = "
						+ p.getLosses() + ", draws = " + p.getDraws()
						+ " where username ='" + p.getName() + "'";
				myStmt.executeUpdate(update);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Updates all the stats for player 1 and 2
	 */
	public void updateStats() {
		p1Stats.setText(" Stats for " + p1.getName() + " ");
		p1Wins.setText(" Won " + p1.getWins() + " times ");
		p1Losses.setText(" Lost " + p1.getLosses() + " times ");
		p1Draws.setText(" Drew " + p1.getDraws() + " times ");
		p1Ratio.setText(" W/L ratio "
				+ new DecimalFormat("##.##").format(p1.getRatio()));
		p1Stats.setFont(new Font("Verdana", 1, 16));
		p1Wins.setFont(statsFont);
		p1Losses.setFont(statsFont);
		p1Draws.setFont(statsFont);
		p1Ratio.setFont(statsFont);

		p2Stats.setText(" Stats for " + p2.getName() + " ");
		p2Wins.setText(" Won " + p2.getWins() + " times ");
		p2Losses.setText(" Lost " + p2.getLosses() + " times ");
		p2Draws.setText(" Drew " + p2.getDraws() + " times ");
		p2Ratio.setText(" W/L ratio "
				+ new DecimalFormat("##.##").format(p2.getRatio()));
		p2Stats.setFont(new Font("Verdana", 1, 16));
		p2Wins.setFont(statsFont);
		p2Losses.setFont(statsFont);
		p2Draws.setFont(statsFont);
		p2Ratio.setFont(statsFont);
	}
	/**
	 * Updating the visuals of the game board
	 * @param gb
	 */
	public void update(GameBoard gb) {
		int rows = gb.getRows();
		int columns = gb.getColumns();
		int col = gb.getIndex();
		for (int k = 0; k < columns; k++) {
			slots[0][k].setOpaque(true);
			slots[0][k].setBackground(Color.white);
		}
		if (player == 1) {
			slots[0][col].setOpaque(true);
			slots[0][col].setBackground(new Color(255, 0, 0));
		} else if (player == -1) {
			slots[0][col].setOpaque(true);
			slots[0][col].setBackground(new Color(255, 255, 110));

		} else {
			slots[0][col].setOpaque(true);
			slots[0][col].setBackground(Color.white);
		}
		int[][] tempGrid = gb.getGrid();
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				if (tempGrid[i][j] == 1) {
					slots[j + 1][i].setOpaque(true);
					slots[j + 1][i].setBackground(Color.RED);
				} else if (gb.getGrid()[i][j] == -1) {
					slots[j + 1][i].setOpaque(true);
					slots[j + 1][i].setBackground(Color.YELLOW);
				} else {
					slots[j + 1][i].setOpaque(true);
					slots[j + 1][i].setBackground(Color.GRAY);
				}
			}
		}

	}
}
