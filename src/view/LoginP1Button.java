package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import logic.Player;
import main.Game;

public class LoginP1Button extends JButton implements ActionListener {

	/**
		 * 
		 */
	private static final long serialVersionUID = -6261685719955815506L;
	private boolean found;

	public LoginP1Button() {
		super("Login");
		addActionListener(this);
		this.setFocusable(false);

	}

	public void actionPerformed(ActionEvent arg0) {
//		Window.STATE = Window.LOGIN_STATE;
		System.out.println("Login Pressed");
		String name = JOptionPane.showInputDialog("Who do you wish to sign in");
		if (name != null) {
			findPlayer(name);
		}
		Window.STATE = Window.PLAY_STATE;

	}

	private void findPlayer(String name) {
		found = false;
		for (int i = 0; i < Game.PLAYERS.size(); i++) {
			Player p = Game.PLAYERS.get(i);
			if (name.equalsIgnoreCase(p.getName())) {
				Window.p1 = p;
				JOptionPane.showMessageDialog(null,
						"Signed in as " + p.getName());
				found = true;
				break;
			}
		}
		if (!found) {
			Player p = new Player(name, 0, 0, 0, 0);
			Game.NEWPLAYERS.add(0, p);
			Game.PLAYERS.add(p);
			Window.p1 = Game.NEWPLAYERS.get(0);
			JOptionPane.showMessageDialog(null, "User not found but created "
					+ p.getName());
		}
	}
}
