package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import logic.Player;
import view.Window;

public class Game {
	public static final int WIDTH = 120, HEIGHT = WIDTH / 3 * 2;
	public static ArrayList<Player> PLAYERS = new ArrayList<Player>();
	public static ArrayList<Player> NEWPLAYERS = new ArrayList<Player>();
	private int COLUMNS = 7, ROWS = 6;

	public Game() {
		Window window = new Window(WIDTH * COLUMNS, HEIGHT * ROWS, COLUMNS,
				ROWS, "Connect four");
	}

	public static void main(String[] args) {
		/**
		 * Loading the database as soon as the program starts up. The params for
		 * the game constructor are easy to edit and arranged so that the game
		 * could be played with a variety of board sizes
		 */

		try {

			Connection myCon = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/Connect4", "admin", "admin");

			Statement myStmt = myCon.createStatement();

			ResultSet myRs = myStmt.executeQuery("select * from players");

			while (myRs.next()) {
				String name = myRs.getString("username");
				int games = myRs.getInt("games");
				int wins = myRs.getInt("wins");
				int losses = myRs.getInt("losses");
				int draws = myRs.getInt("draws");
				PLAYERS.add(new Player(name, games, wins, losses, draws));
			}
			myCon.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Game game = new Game();
	}

}
